
default: build

build:
	@go build -v

test:
	@go test -v

coverage:
	@go test --cover

coverage-report:
	@go test --coverprofile=coverage.out
	@go tool cover -html=coverage.out
	@rm coverage.out
