package logaday

import (
	"bytes"
	"io/ioutil"
	"os"
	"runtime"
	"testing"
	"time"
)

const (
	testDir    = "logs"
	testFormat = "logs/test.{year}{month}{day}.log"

	testPathA = "logs/test.00010101.log"
	testPathB = "logs/test.00010102.log"
)

var (
	testTimeA1 = time.Date(1, 1, 1, 0, 1, 0, 0, time.UTC)
	testTimeA2 = time.Date(1, 1, 1, 0, 2, 0, 0, time.UTC)
	testTimeB1 = time.Date(1, 1, 2, 0, 1, 0, 0, time.UTC)

	testData       = []byte("test")
	testDataLength = len(testData)
)

func cleanWorkspace(t *testing.T) {
	err := os.RemoveAll(testDir)
	if err != nil {
		t.Fatalf("Error cleaning workspace: %s", err)
	}
}

func requireFile(t *testing.T, path string) {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		t.Fatalf("File required %s", path)
	} else if err != nil {
		t.Fatalf("Error checking file %s: %s", path, err)
	}
}

func requireFileContents(t *testing.T, path string, data []byte) {
	fileData, err := ioutil.ReadFile(path)
	if os.IsNotExist(err) {
		t.Fatalf("File required %s", path)
	} else if err != nil {
		t.Fatalf("Error checking file %s: %s", path, err)
	}

	if !bytes.Equal(data, fileData) {
		t.Log("File contents mismatch:")
		t.Logf("  Expected: %v", data)
		t.Logf("  Actual:   %v", fileData)
		t.Errorf("Unexpected file data %s", path)
	}
}

func Test_NewLogger(t *testing.T) {
	tables := []struct {
		valid    bool
		format   string
		expanded string
	}{
		{false, "", ""},
		{false, "logs/test.log", ""},
		{false, "logs/{month}{day}.log", ""},
		{false, "logs/{year}{day}.log", ""},
		{false, "logs/{year}{month}.log", ""},
		{true, "logs/{date}.log", "logs/{year}{month}{day}.log"},
		{true, "logs/{year}{month}{day}.log", ""},
		{true, "logs/{date}/{year}{month}{day}.log", "logs/{year}{month}{day}/{year}{month}{day}.log"},
	}

	for _, table := range tables {
		t.Run(table.format, func(t *testing.T) {
			logger, err := NewLogger(table.format)
			if err == nil && logger == nil {
				t.Errorf("Error and logger are both nil")
			}
			if !table.valid && err == nil {
				t.Errorf("Created logger with invalid format %s", table.format)
			}
			if table.valid && err != nil {
				t.Errorf("Unable to create logger with valid format %s: %s", table.format, err)
			}
			if table.expanded != "" && logger.formatter.format != table.expanded {
				t.Errorf("Expected format mismatch: %s != %s", logger.formatter.format, table.expanded)
			}
		})
	}
}

func Test_GetWriter(t *testing.T) {
	defer runtime.GC()
	defer cleanWorkspace(t)

	cleanWorkspace(t)

	logger, err := NewLogger(testFormat)
	if err != nil {
		t.Fatalf("Error calling NewLogger: %s", err)
	}

	tables := []struct {
		time time.Time
		path string
	}{
		{testTimeA1, testPathA},
		{testTimeB1, testPathB},
	}

	for _, table := range tables {
		t.Run(table.path, func(t *testing.T) {
			_, err = logger.GetWriter(table.time)
			if err != nil {
				t.Fatalf("Error calling logger.GetWriter: %s", err)
			}
			requireFile(t, table.path)
		})
	}
}

func Test_Write(t *testing.T) {
	defer runtime.GC()
	defer cleanWorkspace(t)

	cleanWorkspace(t)

	logger, err := NewLogger(testFormat)
	if err != nil {
		t.Fatalf("Error calling NewLogger: %s", err)
	}

	times := []time.Time{
		testTimeA1,
		testTimeA2,
		testTimeB1,
	}

	for _, time := range times {
		writer, err := logger.GetWriter(time)
		if err != nil {
			t.Fatalf("Error calling logger.GetWriter: %s", err)
		}

		count, err := writer.Write(testData)
		if count != testDataLength {
			t.Fatalf("Wrote wrong number of bytes: %d != %d", count, testDataLength)
		}
	}

	testDataA := make([]byte, 0)
	testDataA = append(testDataA, testData...)
	testDataA = append(testDataA, testData...)
	requireFileContents(t, testPathA, testDataA)

	testDataB := testData
	requireFileContents(t, testPathB, testDataB)
}

func newAndWrite(t *testing.T, time time.Time) {
	logger, err := NewLogger(testFormat)
	if err != nil {
		t.Fatalf("Error calling NewLogger: %s", err)
	}

	writer, err := logger.GetWriter(time)
	if err != nil {
		t.Fatalf("Error calling logger.GetWriter: %s", err)
	}

	count, err := writer.Write(testData)
	if count != testDataLength {
		t.Fatalf("Wrote wrong number of bytes: %d != %d", count, testDataLength)
	}
}

func Test_WriteAppend(t *testing.T) {
	defer runtime.GC()
	defer cleanWorkspace(t)

	cleanWorkspace(t)

	newAndWrite(t, testTimeA1)
	newAndWrite(t, testTimeA2)

	testDataA := make([]byte, 0)
	testDataA = append(testDataA, testData...)
	testDataA = append(testDataA, testData...)
	requireFileContents(t, testPathA, testDataA)
}
