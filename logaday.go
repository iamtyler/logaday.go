package logaday

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"time"
)

const (
	keyDate  = "{date}"
	keyYear  = "{year}"
	keyMonth = "{month}"
	keyDay   = "{day}"

	errorMessageNotPrepared = "File not open, be sure to prepare first"
	errorMessageFormatEmpty = "Format must not be empty"
)

var (
	keyDateParts = fmt.Sprintf("%s%s%s", keyYear, keyMonth, keyDay)

	errorMessageKeysMissing = fmt.Sprintf("Format missing subsitution keys %s %s %s or %s", keyYear, keyMonth, keyDay, keyDate)
)

type pathFormatter struct {
	format string
}

func newPathFormatter(format string) (*pathFormatter, error) {
	if len(format) == 0 {
		return nil, errors.New(errorMessageFormatEmpty)
	}

	hasAllParts := strings.Contains(format, keyYear) && strings.Contains(format, keyMonth) && strings.Contains(format, keyDay)
	hasDate := strings.Contains(format, keyDate)
	if !hasAllParts && !hasDate {
		return nil, errors.New(errorMessageKeysMissing)
	}

	if hasDate {
		format = strings.Replace(format, keyDate, keyDateParts, -1)
	}

	return &pathFormatter{format}, nil
}

func (g *pathFormatter) getPath(t time.Time) string {
	// Serialize start date parts to string
	year := fmt.Sprintf("%04d", t.Year())
	month := fmt.Sprintf("%02d", t.Month())
	day := fmt.Sprintf("%02d", t.Day())

	// Buid path from format
	path := strings.Replace(g.format, keyYear, year, -1)
	path = strings.Replace(path, keyMonth, month, -1)
	path = strings.Replace(path, keyDay, day, -1)

	return path
}

// LogFile represents an individual log file, providing an io.Writer implementation.
type LogFile struct {
	path  string
	file  *os.File
	start time.Time
	end   time.Time
}

func finalizeLogFile(l *LogFile) {
	l.file.Close()
}

func (f *LogFile) contains(t time.Time) bool {
	return t.After(f.start) && t.Before(f.end)
}

func (f *LogFile) Write(data []byte) (int, error) {
	return f.file.Write(data)
}

// Logger is a set of logging configuration, including the path format and time location.
type Logger struct {
	formatter pathFormatter
	location  *time.Location

	file *LogFile

	mutex sync.Mutex
}

// NewLogger creates a logger with UTC time location
func NewLogger(format string) (*Logger, error) {
	return NewLocalLogger(format, time.UTC)
}

// NewLocalLogger creates a logger with the provided time location
func NewLocalLogger(format string, location *time.Location) (*Logger, error) {
	formatter, err := newPathFormatter(format)
	if err != nil {
		return nil, err
	}

	return &Logger{
		formatter: *formatter,
		location:  location,
		file:      nil,
	}, nil
}

// GetWriter returns a reference to an io.Writer for the log file corresponding to the provided time.
func (l *Logger) GetWriter(t time.Time) (io.Writer, error) {
	// Acquire lock
	l.mutex.Lock()
	defer l.mutex.Unlock()

	// If in range, do nothing
	if l.file != nil && l.file.contains(t) {
		return l.file, nil
	}
	l.file = nil

	// Convert to specified location
	t = t.In(l.location)

	// Generate new start time
	start := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, l.location)

	// Generate path
	path := l.formatter.getPath(start)

	// Ensure parent directories exist
	parent := filepath.Dir(path)
	err := os.MkdirAll(parent, 0755)
	if err != nil {
		return nil, fmt.Errorf("Unable to create dir %s", parent)
	}

	// Open the file
	file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return nil, err
	}

	l.file = &LogFile{
		path:  path,
		file:  file,
		start: start,
		end:   start.AddDate(0, 0, 1),
	}
	runtime.SetFinalizer(l.file, finalizeLogFile)

	return l.file, nil
}
