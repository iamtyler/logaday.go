# logaday [![pipeline status](https://gitlab.com/iamtyler/logaday.go/badges/master/pipeline.svg)](https://gitlab.com/iamtyler/logaday.go/commits/master) [![coverage report](https://gitlab.com/iamtyler/logaday.go/badges/master/coverage.svg)](https://gitlab.com/iamtyler/logaday.go/commits/master)

**logaday** is a Go package for writing log messages to files by date. When 
using this package, a new file will be written for each day a log message is 
recorded.

## Example

    package main

    import (
        "fmt"
        "time"

        "gitlab.com/iamtyler/logaday.go"
    )

    func log(logger *logaday.Logger, message string) {
        now := time.Now().UTC()
        line := fmt.Sprintf("[%s] %s\n", now.Format(time.RFC3339), message)
        writer, _ := logger.GetWriter(now)
        writer.Write([]byte(line))
    }

    func main() {
        logger, _ := logaday.NewLogger("logs/example.{date}.log")
        log(logger, "today is a new day")
    }
